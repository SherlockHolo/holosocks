#!/usr/bin/env python3

import asyncio
import functools
import logging
from sys import argv, exit

logging.basicConfig(
    level=logging.INFO,
    format='{asctime} {levelname} {message}',
    datefmt='%Y-%m-%d %H:%M:%S',
    style='{')


class Server:
    def __init__(self, target_addr, target_port):
        self.target_addr = target_addr
        self.target_port = target_port
        logging.debug("target: {}:{}".format(self.target_addr, self.target_port))

    async def handle(self, reader, writer):
        try:
            r_reader, r_writer = await asyncio.open_connection(self.target_addr, self.target_port)
            # self.writer = writer
            # self.r_writer = r_writer

        except:
            writer.close()
            return None

        logging.debug('connected to target')

        relay1 = asyncio.ensure_future(self.relay(reader, r_writer))
        relay2 = asyncio.ensure_future(self.relay(r_reader, writer))
        # logging.debug('start relay')

        relay1.add_done_callback(functools.partial(self.close_transport, writer, r_writer))
        relay2.add_done_callback(functools.partial(self.close_transport, writer, r_writer))

    async def relay(self, fr, to):
        try:
            while True:
                data = await fr.read(4096)
                if not data:
                    break
                to.write(data)
                await to.drain()
                logging.debug('relay data')

        except OSError as e:
            logging.error(e)
            return None

    def close_transport(self, writer, r_writer, future):
        writer.close()
        r_writer.close()


def main():
    try:
        import uvloop
        asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
    except ImportError:
        pass

    if len(argv) == 1:
        exit('{} listen_addr listen_port target_addr target_port'.format(argv[0]))

    elif len(argv) != 5:
        exit('error arguments')

    logging.debug(argv)

    loop = asyncio.get_event_loop()
    server = Server(argv[3], int(argv[4]))
    core = asyncio.start_server(server.handle, argv[1], int(argv[2]), loop=loop)
    serve = loop.run_until_complete(core)

    try:
        loop.run_forever()

    except KeyboardInterrupt:
        pass

    serve.close()
    loop.run_until_complete(serve.wait_closed())
    loop.close()


if __name__ == '__main__':
    main()
