#!/usr/bin/env python3

import aiodns
import asyncio
import functools
import logging
import socket
import struct

logging.basicConfig(
    level=logging.DEBUG,
    format='{asctime} {levelname} {message}',
    datefmt='%Y-%m-%d %H:%M:%S',
    style='{')


class Resolver:
    def __init__(self, maxsize=128, nameservers=None):
        try:
            from cachetools import LRUCache
            self.cache = LRUCache(maxsize=maxsize)
            logging.info('use LRUCache')
        except ImportError:
            self.resolve = self._resolve
        self.resolver = aiodns.DNSResolver(nameservers=nameservers)

    async def _resolve(self, host):
        try:
            result = await self.resolver.gethostbyname(host, socket.AF_INET6)
            if not result.addresses:
                raise aiodns.error.DNSError
        except aiodns.error.DNSError:
            result = await self.resolver.gethostbyname(host, socket.AF_INET)

        return result.addresses[0]

    async def resolve(self, host):
        try:
            return self.cache[host]
        except KeyError:
            ip_addr = await self._resolve(host)
            self.cache[host] = ip_addr
            return ip_addr


class Server:
    def __init__(self):
        resolver = Resolver()
        self.resolve = resolver.resolve

    async def handle(self, reader, writer):
        logging.debug(
            'connect from {}'.format(writer.get_extra_info('peername')))
        request = await reader.read(2)
        if request[0] != 5:
            writer.close()
            logging.error('socks version not support')
            return None
        else:
            nmethods = request[1]
            logging.debug('methods number: {}'.format(nmethods))
            methods = await reader.read(nmethods)
            if 0 in methods:
                writer.write(b'\x05\x00')
                await writer.drain()
            else:
                writer.write(b'\x05\xff')
                logging.error('Authentication not support')
                writer.close()
                return None

        data = await reader.read(4)
        ver, cmd, rsv, atyp = data
        if cmd != 1:
            data = []
            data.append(b'\x05\x07\x00\x01')
            data.append(socket.inet_aton('0.0.0.0'))
            data.append(struct.pack('>H', 0))
            writer.write(b''.join(data))
            writer.close()
            logging.error('cmd not support')
            return None
        else:
            logging.debug('atyp is {}'.format(atyp))
            if atyp == 1:
                _addr = await reader.read(4)
                addr = socket.inet_ntoa(_addr)

            elif atyp == 3:
                addr_len = await reader.read(1)
                addr = await reader.read(ord(addr_len))
                logging.debug('domain name is {}'.format(addr))
                addr = await self.resolve(addr)

            elif atyp == 4:
                _addr = await reader.read(16)
                addr = socket.inet_ntop(socket.AF_INET6, _addr)

            _port = await reader.read(2)
            port = struct.unpack('>H', _port)[0]
            logging.debug('remote: {}:{}'.format(addr, port))

            try:
                r_reader, r_writer = await asyncio.open_connection(addr, port)
            except:
                data = []
                data.append(b'\x05\x04\x00\x01')
                data.append(socket.inet_aton('0.0.0.0'))
                data.append(struct.pack('>H', 0))
                writer.write(b''.join(data))
                writer.close()
                return None

            bind_info = r_writer.get_extra_info('sockname')
            data = []
            data.append(b'\x05\x00\x00')

            try:
                bind_addr = socket.inet_pton(socket.AF_INET6, bind_info[0])
                data.append(b'\x04')

            except OSError:
                bind_addr = socket.inet_aton(bind_info[0])
                data.append(b'\x01')

            data.append(bind_addr)
            data.append(struct.pack('>H', bind_info[1]))

            writer.write(b''.join(data))
            await writer.drain()

            async def relay(fr, to):
                try:
                    while True:
                        data = await fr.read(8192)
                        if not data:
                            break
                        to.write(data)
                        await to.drain()
                except OSError as e:
                    logging.error(e)
                    return None

            logging.debug('start relay')

            s2r = asyncio.ensure_future(relay(reader, r_writer))
            r2s = asyncio.ensure_future(relay(r_reader, writer))

            s2r.add_done_callback(
                functools.partial(self.close_transport, writer, r_writer))

            r2s.add_done_callback(
                functools.partial(self.close_transport, writer, r_writer))

    def close_transport(self, writer, r_writer, future):
        writer.close()
        r_writer.close()


def main():
    try:
        import uvloop
        asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
    except ImportError:
        pass

    loop = asyncio.get_event_loop()
    s = Server()
    coro = asyncio.start_server(s.handle, '127.0.0.2', 1089, loop=loop)
    server = loop.run_until_complete(coro)

    try:
        loop.run_forever()

    except KeyboardInterrupt:
        pass

    server.close()
    loop.run_until_complete(server.wait_closed())
    loop.close()


if __name__ == '__main__':
    main()
